MiniMenus
===============================================================================

MiniMenus is a module designed to provide Google-like registration and login links in the top right corner of a Drupal website for anonymous users, and a mini-control-panel for authenticated users and administrators.

MiniMenus integrates with the Administration (only in the Drupal 4.7.x version) and Masquerade modules if they are installed and enabled.

MiniMenus development has been sponsored by CivicSpaceLabs.org.  After May 10, 2007 the module has been maintained by Robin Monks, <robin @ civicspacelabs . org>.
